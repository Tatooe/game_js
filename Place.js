class Place {
  constructor(title, description) {
    this.title = title;
    this.description = description;
    this.newLine = spacer.newLine();
    this.items = [];
    this.exits = [];
  }

  getTitlePlace = () => spacer.box(this.title, this.title.length + 4, "=");

  getDescription = () => this.description;

  getInfoPlace() {
    return `Vous êtes dans ${this.getTitlePlace()} \n 
    ${this.getDescription()}\n    
    ${this.getItemsInfo()} \n 
    ${this.getExitsInfo()}`;
  }

  getExitsInfo() {
    let exitsString = "Sortir de " + this.title;
    exitsString += ":" + "\n"; // newLine

    Object.keys(this.exits).forEach(function (key) {
      exitsString += "   - " + key;
      exitsString += "\n"; // newLine
    });
    return exitsString;
  }

  showInfo = () => console.log(this.getInfoPlace());

  addItem = (itemAdd) => this.items.push(itemAdd);

  getItemsInfo() {
    var string = `Items:` + this.newLine;
    for (let i = 0; i < this.items.length; i++) {
      string += `-${this.items[i]}\n`;
    }
    return string;
  }

  addExit = (direction, exit) => (this.exits[direction] = exit);

  getExit = (direction) => this.exits[direction];

  getLastItem = () => this.items.pop();
}
