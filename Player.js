class Player {
  constructor(name, health) {
    this.name = name;
    this.health = health;
    this.newLine = spacer.newLine();
    this.place = "";
    this.items = [];
  }

  getNamePlayer = () => this.name;

  getHealthPlayer = () => this.health;

  getItemsPlayer() {
    var string = `Items: \n`;
    for (let i = 0; i < this.items.length; i++) {
      string += `-${this.items[i]}\n`;
    }
    return string;
  }

  getTitleInfo = () => this.getNamePlayer() + " " + this.getHealthPlayer();

  getPlayerPlace = () => this.place;

  showPlayerHealth = () =>
    `${this.getNamePlayer()} a une santé de  ${this.getHealthPlayer()}`;

  getPlayerInfo = () => {
    let info = spacer.box(this.getTitleInfo(), 40, "*");
    info += "  " + this.getItemsPlayer();
    info += spacer.line(40, "*");
    info += "\n"; //newLine

    return info;
  };

  addPlayerItem = (item) => this.items.push(item);

  setPlayerPlace = (destination) => (this.place = destination);

  getPlayerPlace = () => this.place;

  showInfo(character) {
    console.log(this.getPlayerInfo(character));
  }
}
