let render = function () {
  console.clear();
  player.getPlayerPlace().showInfo();
  player.showInfo();
};

let go = function (direction) {
  var place = player.getPlayerPlace();
  var destination = place.getExit(direction);
  player.setPlayerPlace(destination);
  render();
  return "";
};

let get = function () {
  var place = player.getPlayerPlace();
  var item = place.getLastItem();
  player.addPlayerItem(item);
  render();
  return "";
};
